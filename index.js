/**
 * Impor HTTP Standar Library dari Node.js
 * Hal inilah yang nantinya akan kita gunakan untuk membuat HTTP Server
 */
  // import module
 const http = require('http');
 const fs = require('fs');
 const path = require('path');
 // import atau panggil dotenv module (third party module)
 require('dotenv').config();
 const PORT = process.env.PORT;
 
//  import file data dan filterdata
 const data = require("./data.js");
 const {byageAndBanana, byAgeAndFemale, byEyeAndAge, byCompanyAndEye, 
        byRegistAndActive} = require("./filterData.js");
// deklarasi variabel untuk directory
 const PUBLIC_DIRECTORY = path.join(__dirname, 'public');
 const ASSETS_DIRECTORY = path.join(__dirname, 'public/assets')
 
//  fungsi untuk mencari dan membaca file html
 function getHTML(htmlFileName) {
   const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
   return fs.readFileSync(htmlFilePath, 'utf-8');
 }
 //  fungsi untuk mencari dan membaca file css atau yang ada di asssets
 function getAssets(assetsFileName){
   const assetsFilePath = path.join(ASSETS_DIRECTORY, assetsFileName);
   return fs.readFileSync(assetsFilePath, 'utf-8');
 }
// fungsi untuk merubah kedalam bentuk json
 function toJSON(value) {
   return JSON.stringify(value);
 }

  // Fungsi yang berjalan ketika ada request yang masuk.
 function onRequest(req, res) {
   switch (req.url) {
     case "/":
       res.writeHead(200, {'Content-Type' : 'text/html'});
       res.end(getHTML("index.html"));
       return;
     case "/assets/style.css":
       res.setHeader("Content-Type", "text/css");
       res.end(getAssets("style.css"));
       return;
     case "/about":
       res.writeHead(200, {'Content-Type' : 'text/html'});
       res.end(getHTML("about.html"));
       return;
     case "/data":
       const responseJSON = toJSON(data)
        res.setHeader("Content-Type", "application/json");
       res.writeHead(200);
       res.end(responseJSON);
       return;
     case "/data1":
       res.setHeader("Content-Type", "application/json");
       res.writeHead(200);
       res.end(toJSON(byageAndBanana));
       return;
     case "/data2":
        res.setHeader("Content-Type", "application/json");
       res.writeHead(200);
       res.end(toJSON(byAgeAndFemale));
       return;
     case "/data3":
       res.setHeader("Content-Type", "application/json");
       res.writeHead(200);
       res.end(toJSON(byEyeAndAge));
       return;
     case "/data4":
       res.setHeader("Content-Type", "application/json");
       res.writeHead(200);
       res.end(toJSON(byCompanyAndEye));
       return;
     case "/data5":
       res.setHeader("Content-Type", "application/json");
       res.writeHead(200);
       res.end(toJSON(byRegistAndActive));
       return;
     case "/assets/halaman_404.png":
       const img = path.join(ASSETS_DIRECTORY, "halaman_404.png"); 
       fs.readFile(img , function(err, data){
          if (err) throw err;
            res.writeHead(404,{'Content-Type' : 'image/jpeg'});
            res.end(data)
       });
       return;
     default:
      res.writeHead(404, {'Content-Type' : 'text/html'});
      res.end(getHTML("404.html"));
      return;;
   }
 }
 
 const server = http.createServer(onRequest);
// Jalankan server
server.listen(PORT, '0.0.0.0', () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})

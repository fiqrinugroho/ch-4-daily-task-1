const data = require("./data.js");

// fungsi untuk mengecek apakah resultu kosong atau tidak
function resHandler(value){
    if (!value.length){
        // jika resultnya kosong maka akan menampilkan message ini
        const err = { message : "data tidak ada/tidak ditemukan"}
        return err
    }else {
        // jika data tidak kosong maka akan di tampilkan total data dan datanya
        let dataJson = {
            "totalData": value.length,
            "allData": value
        };
        return dataJson
    }
}

function byageAndBanana() {
    // Tempat penampungan hasil
    const result = [];
    // Menggunakan looping
    for (let  i = 0; i < data.length; i++) {
      // age nya dibawah 30 tahun dan favorit buah nya pisang
      if(data[i].age <= 30 && data[i].favoriteFruit === "banana"){
          result.push(data[i]);
      }
    }
 return resHandler(result);
}


function byAgeAndFemale(){
    // Tempat penampungan hasil
    const result = [];
    // Menggunakan looping
    for (let  i = 0; i < data.length; i++) {
      // gender nya female atau company nya FSW4 dan age nya diatas 30 tahun 
      if(data[i].gender === "female" || data[i].company === "FSW4"){
        if (data[i].age >= 30){
          result.push(data[i]);
        }
      }
    }
 return resHandler(result);
}

  function byEyeAndAge(){
    // Tempat penampungan hasil
    const result = [];
    // Menggunakan looping
    for (let  i = 0; i < data.length; i++) {
        // warna mata biru dan age nya diantara 35 sampai dengan 40, dan favorit buah nya apel 
      if(data[i].eyeColor === "blue"){
        if (data[i].age >= 35 && data[i].age <= 45){
          result.push(data[i]);
        }
      }
    }
 return resHandler(result);
}

function byCompanyAndEye(){
    // Tempat penampungan hasil
    const result = [];
    // Menggunakan looping
    for (let  i = 0; i < data.length; i++) {
      // company nya Pelangi atau Intel, dan warna mata nya hijau
      if(data[i].company === "Pelangi" || data[i].company === "Intel"){
        if (data[i].eyeColor === "green"){
          result.push(data[i]);
        }
      }
    }
 return resHandler(result);
}

function byRegistAndActive(){
    // Tempat penampungan hasil
    const result = [];
    // Menggunakan looping
    for (let  i = 0; i < data.length; i++) {
        // deklarasi variabel years sebagai variabel yg menyimpan hasil potongan tahun
        // variabel remaining menyimpan sisa data setelah - 
        const [years, remaining] = data[i].registered.split("-");
        // registered di bawah tahun 2016 dan masih active(true)
        if( years < 2016 && data[i].isActive){
            result.push(data[i]);
        }
    }
 return resHandler(result);
}

//   memanggil fungsi 
  byageAndBanana = byageAndBanana();
  byCompanyAndEye = byCompanyAndEye();
  byEyeAndAge = byEyeAndAge();
  byRegistAndActive = byRegistAndActive();
  byAgeAndFemale = byAgeAndFemale();

// export fungsi
  module.exports =  {byageAndBanana, byCompanyAndEye, byEyeAndAge, byRegistAndActive, byAgeAndFemale};